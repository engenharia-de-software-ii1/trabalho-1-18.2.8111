<?php
    function veriSoma($x, $y, $z){
        if(
            // Associatividade
            ($x + $y) + $z == $x + ($y + $z) &&
             // Comutatividade
            ($x + $y == $y + $x) &&
            // Elemento neutro
            ($x + 0 == 0 && $y + 0 == 0 || $z + 0 == 0) &&
            // Simétrico
            ($x + (-$x) == 0 && $y + (-$y) == 0 && $z + (-$z) == 0)
            ){
            return true;
        }
        return false;
    }

    function veriProduto($x, $y, $z){
        if(
            // Associativa
            ($x * $y) * $z == $x * ($y * $z) &&
            // Comutativa
            ($x * $y == $y * $x) &&
            // Elemento Neutro
            ($x * 1 == 1 && $y * 1 == 1 && $z * 1 == 1) &&
            // Inverso Multiplicativo
            (Math.pow($x , -1) == 1 )
            ){
            return true;
        }
        return false;
    }

    function Soma($x, $y){
        $z = $x + $y;
        if(veriSoma($x, $y, $z)){
            return "Erro! Propriedade dos axiomas de multiplicação violada";
        }
        return $z;
    }

    function Produto($x, $y){
        $z = $x * $y;
        if(veriProduto($x, $y, $z)){
            return "Erro! Propriedade dos axiomas de soma violada";
        }
        return $z;
    }

    $soma = Soma(5 , 5);
    $produto = Produto(5 , 5);
    echo "Soma: $soma"; 
    echo " Produto: $produto";
?>
